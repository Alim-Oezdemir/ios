# 1.5.0-dev
- Data access notification
- Switch to base32

# Release 1.4.2
- List of 3rd party libraries in settings

In this update we have included our 3rd party libraries in the luca settings.
